# Simple Tween

Simple Tween is a tool for Unity that accelerates the process of creating and prototyping games. 
It's small script but it can make your life as game developer muuuuch easier. 
You can scale, move, rotate, lerp colors, values, create inline automatic coroutines and much more in time by one line of code.

## 1. Construction

    Tween.ExampleMethod(dynamic main, Func<dynamic> target, float duration, float delay, AnimationCurve curve);
    
Available methods: Move, Scale, Rotate, Color, Custom, Wait.
    
## 2. Usage

Simply put Tween.cs in your project and use tween methods :)
    
## 3. Examples

#### Simple tween
    Tween.Move(someTransform, new Vector3(2, 3, 4), 2f);
Moves someTransform to Vector3(2, 3, 4) within 2 seconds
___
#### Tween with delay and animation curve
    Tween.Move(someTransform, new Vector3(2, 3, 4), 2f, 1f, Tween.Curve.Linear);
Moves someTransform to Vector3(2, 3, 4) within 2 seconds after 1 second delay by linear curve;
___
#### Parameter 'active' - updated target
    Tween.Move(someTransform, () => anotherTransform.position + new Vector3(2,2,3), 2f, active: true);
Moves someTransform calculating their target every frame.
Active parameter can be used for almost all tween methods.
___
#### Tween with advanced expression
    Tween.Move(someTransform, () => 
        {
            float someValue = 3f;
            Vector3 someVector = new Vector3(1,2,3);
            float pong = Mathf.PingPong(Time.time, 1f);

            return someVector * someValue * pong + anotherTransform;
        } , 2f);
As above but with more advanced function.
___
#### One line wait
    Tween.Wait(2f, () => someValue = 2 + 2);
Waits 2 seconds to execute one line of code in current void.
___
#### Advanced wait
    Tween.Wait(5f, () => 
    { 
        someValue = 2 + 2
        anotherValue = 100;
        pi = 3.14f;
    });
Waits 5 seconds to execute code in current void.
___
#### Queue
    Tween.Next(0.5f, () => 
    { 
        spawnedObject.Show();
        spawnedObject.Initialize();
    });
    
    Tween.Next(0.5f, () => 
    { 
        spawnedObject.Animate();
    });
    
    Tween.Next(1f, () => spawnedObject.Destroy() );
Tween.Next is just method to create a delayed sequences of actions.
___
#### Queue reset
    Tween.ResetQueue();
Resets Tween.Next queue.
___
#### Custom
    Tween.Custom("MakeMeRich", 2f, 1f, () => money += Time.deltaTime );
Example custom method which after 1 second delay adds money by Time.deltaTime for 2 secounds.
    
    Tween.Custom("Increase", 5f, 0f, () => { 
        gameAreaSize += Time.deltaTime * 2f;
        difficulty += Time.deltaTime * difficultyScale;
    } );
Example custom method which increase some values for 5 seconds without delay.


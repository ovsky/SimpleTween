using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;

namespace SimpleTween
{
    public class Tween : MonoBehaviour
    {
        static List<TweenObject> tweenObjects = new List<TweenObject>();
        static Tween instance;

#region Functions
        
        //move
        public static void Move(Transform transform, Transform target, float duration, float delay = 0f, AnimationCurve curve = null, bool active = true)
        {
            Move(transform, () => target.position, duration, delay, curve, active);
        }

        public static void Move(Transform transform, Func<Vector3> function, float duration, float delay = 0f, AnimationCurve curve = null, bool active = false)
        {
            Vector3 target = active ? default : function();
            Wait(delay, () =>
            {
                TweenAction action = new TweenAction(transform, transform.position, duration, "Move", curve ?? Curve.Default);
                action.function = () => transform.position = Vector3.Lerp(action.start, active ? function() : target, action.curve.Evaluate(action.timer));

                TweenObject.Set(transform, action);
            });
        }
        
        //move
        public static void Rotate(Transform transform, Transform target, float duration, float delay = 0f, AnimationCurve curve = null, bool active = true)
        {
            Rotate(transform, () => target.rotation, duration, delay, curve, active);
        }

        public static void Rotate(Transform transform, Func<Quaternion> function, float duration, float delay = 0f, AnimationCurve curve = null, bool active = false)
        {
            Quaternion target = active ? default : function();
            Wait(delay, () =>
            {
                TweenAction action = new TweenAction(transform, transform.position, duration, "Move", curve ?? Curve.Default);
                action.function = () => transform.position = Quaternion.Lerp(action.start, active ? function() : target, action.curve.Evaluate(action.timer));

                TweenObject.Set(transform, action);
            });
        }

        //scale
        public static void Scale(Transform transform, Func<Vector3> function, float duration, float delay = 0f, AnimationCurve curve = null, bool active = false)
        {
            Vector3 target = active ? default : function();
            Wait(delay, () =>
            {
                TweenAction action = new TweenAction(transform, transform.localScale, duration, "Scale", curve ?? Curve.Default);
                action.function = () => transform.localScale = Vector3.Lerp(action.start, active ? function() : target, action.curve.Evaluate(action.timer));

                TweenObject.Set(transform, action);
            });
        }

        //text color
        public static void Color(Text text, Func<Color> function, float duration, float delay = 0f, AnimationCurve curve = null, bool active = false)
        {
            Color target = active ? default : function();
            Wait(delay, () =>
            {
                TweenAction action = new TweenAction(text, text.color, duration, "TextColor", curve ?? Curve.Default);
                action.function = () => text.color = UnityEngine.Color.Lerp(action.start, active ? function() : target, action.curve.Evaluate(action.timer));

                TweenObject.Set(text, action);
            });
        }

        //image color
        public static void Color(Image image, Func<Color> function, float duration, float delay = 0f, AnimationCurve curve = null, bool active = false)
        {
            Color target = active ? default : function();
            Wait(delay, () =>
            {
                TweenAction action = new TweenAction(image, image.color, duration, "ImageColor", curve ?? Curve.Default);
                action.function = () => image.color = UnityEngine.Color.Lerp(action.start, active ? function() : target, action.curve.Evaluate(action.timer));

                TweenObject.Set(image, action);
            });
        }
        
        //custom
        public static void Custom(Action function, float duration, float delay = 0f, AnimationCurve curve = null)
        {
            Wait(delay, () =>
            {
                TweenAction action = new TweenAction(null, null, duration, "Custom", curve ?? Curve.Default);
                action.function = function;

                TweenObject.Set(null, action);
            });
        }
#endregion
#region  Logic

        [RuntimeInitializeOnLoadMethod]
        static void OnLoad()
        {
            if (Tween.instance == null)
            {
                GameObject tweenHolder = new GameObject().AddComponent<Tween>().gameObject;
                tweenHolder.name = "TweenSystem";
                tweenObjects.Clear();
                DontDestroyOnLoad(tweenHolder);
            }
        }

        void Awake() { instance = this; }

        void Update()
        {
            TweenObject singleTweenObject;
            for (int i = 0; i < tweenObjects.Count; i++)
            {
                singleTweenObject = tweenObjects[i];
                foreach (TweenAction tweenAction in singleTweenObject.tweenActions)
                {
                    tweenAction.timer = (Time.time - tweenAction.startTime) / tweenAction.duration;

                    if (tweenAction.outdated)
                    {
                        if (singleTweenObject.tweenActions.All(x => x.outdated == true))
                            tweenObjects.Remove(singleTweenObject);
                    }

                    if (tweenAction.timer >= 1f)
                    {
                        tweenAction.outdated = true;
                        tweenAction.timer = 1f;
                    }

                    tweenAction.function();
                }
            }
        }
        
#endregion
#region  Classes

        class TweenObject
        {
            public object targetObject;
            public List<TweenAction> tweenActions = new List<TweenAction>();

            public static void Set(object targetObject, TweenAction action)
            {
                TweenObject targetTween = null;
                foreach (TweenObject singleTween in tweenObjects)
                {
                    if (singleTween.targetObject == targetObject)
                    {
                        targetTween = singleTween;
                        break;
                    }
                }
                if (targetTween == null)
                {
                    targetTween = new TweenObject();
                    tweenObjects.Add(targetTween);
                }

                targetTween.targetObject = targetObject;
                targetTween.tweenActions.Add(action);
            }
        }

        class TweenAction
        {
            public Action function;
            public dynamic carriedObject;
            public dynamic start;

            public string key;

            public float duration;
            public float startTime;
            public float timer;

            public bool outdated;

            public AnimationCurve curve;

            public TweenAction() { }
            public TweenAction(dynamic carriedObject, dynamic start, float duration, string key, AnimationCurve curve)
            {
                this.carriedObject = carriedObject;
                this.start = start;
                this.duration = duration;
                this.key = key;
                this.curve = curve;
                startTime = Time.time;
            }
        }
        
#endregion
#region  Misc

        public static class Curve
        {
            public static AnimationCurve defaultCurve = EaseInOut;
            public static AnimationCurve Linear => AnimationCurve.Linear(0f, 0f, 1f, 1f);
            public static AnimationCurve EaseInOut => AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
            public static AnimationCurve Constant => AnimationCurve.Constant(0f, 1f, 1f);

            public static AnimationCurve Default => defaultCurve;
            
            public static void SetDefault(AnimationCurve newDefault)
            {
                defaultCurve = newDefault;
            }
        }

        static float queueTimer = 0f;

        public static void Next(float seconds, Action action)
        {
            queueTimer += seconds;
            instance.StartCoroutine(WaitCoroutine(queueTimer, action));
        }

        public static void ResetQueue()
        {
            queueTimer = 0f;
        }

        public static void Wait(float seconds, Action action)
        {
            instance.StartCoroutine(WaitCoroutine(seconds, action));
        }

        static IEnumerator WaitCoroutine(float time, Action callback)
        {
            yield return new WaitForSeconds(time);
            callback();
        }

        public static void Stop(object target, string key = null)
        {
            for (int i = 0; i < tweenObjects.Count; i++)
            {
                if (tweenObjects[i].targetObject == target)
                {
                    if (key == null)
                    {
                        tweenObjects.RemoveAt(i);
                        return;
                    }
                    else
                    {
                        for (int k = 0; k < tweenObjects[i].tweenActions.Count; k++)
                        {
                            if (tweenObjects[i].tweenActions[k].key == key)
                            {
                                tweenObjects[i].tweenActions.RemoveAt(k);
                            }
                        }
                    }
                }
            }
        }
    }
    /*
    public struct F
    {
        public Func<dynamic> action;

        public F(Func<dynamic> action = null)
        {
            this.action = action;
        }
        public static implicit operator F(Func<dynamic> d) => new F(d);
    }
    */

    public static class Extensions
    {
        public static U[] MakeArray<T, U>(this IEnumerable<T> @enum, Func<T, U> rule)
        {
            return @enum.Select(rule).ToArray();
        }
    }
#endregion
}
